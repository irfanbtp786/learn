<?php

/**
* 
*/
class Blog extends CI_Controller
{
	
	
	public function index()
	{
		$this->load->model('authenticate','auth');
		$data['users'] = $this->auth->authenticate();
		
		$this->load->view('blog_index.php',$data);
	}
}
?>