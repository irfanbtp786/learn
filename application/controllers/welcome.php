<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->helper('form');
        $this->load->helper('url'); //Loading url helper
		$this->load->model('test_model');
	}

	public function index()
	{
		$name = $this->input->post('name');
		$pass = $this->input->post('pass');
		if ($name && $pass) {
			$arr = array(
			'name'=>$name,
			'password'=>$pass
			);
			$this->test_model->test($arr);
		}
		$this->load->view('home');
	}

	public function test(){
		echo "Hello";
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */